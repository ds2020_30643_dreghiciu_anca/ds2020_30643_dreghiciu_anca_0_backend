package ro.tuc.ds2020.entities;


import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.persistence.metamodel.IdentifiableType;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "patient")
public class Patient {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "patientId", unique = true, nullable = false)
    private Integer patientId;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "birthDate", nullable = false)
    private String birthDate;

    @Column(name = "gender")
    private String gender;

    @Column(name = "address")
    private String address;

    @Column(name = "medicalRecord")
    private String medicalRecord;

    @ManyToOne
    @JoinColumn
    private Caregiver caregiver;

    @ManyToOne
    @JoinColumn
    private Doctor doctor;

    @OneToOne
    @JoinColumn
    private Person person;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "patient")
    private List<MedicationPlan> medicationPlan;

    public Patient() {
    }

    public Patient(Integer patientId, String name, String birthDate, String gender, String address, String medicalRecord,
                   Caregiver caregiver, Doctor doctor, ArrayList<MedicationPlan> medicationPlan) {
        this.patientId = patientId;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.caregiver = caregiver;
        this.doctor = doctor;
        this.medicationPlan = medicationPlan;
    }

    public Patient(Integer patientId, String name, String birthDate, String gender, String address, String medicalRecord, Caregiver caregiver, Doctor doctor, Person person, ArrayList<MedicationPlan> medicationPlan) {
        this.patientId = patientId;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.caregiver = caregiver;
        this.doctor = doctor;
        this.person = person;
        this.medicationPlan = medicationPlan;
    }

    public Patient(Integer patientId, String name, String birthDate, String gender, String address, String medicalRecord) {
        this.patientId = patientId;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
    }

    public Patient(Integer patientId, String name, String birthDate, String gender, String address, String medicalRecord, Caregiver caregiver, Doctor doctor) {
        this.patientId = patientId;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.caregiver = caregiver;
        this.doctor = doctor;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public List<MedicationPlan> getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(List<MedicationPlan> medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
