package ro.tuc.ds2020.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medicationPlan")
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "medicationPlanId", unique = true, nullable = false)
    private Integer medicationPlanId;

    @Column(name = "intakeInterval")
    private String intakeInterval;

    @Column(name = "startDate")
    private String startDate;

    @Column(name = "endDate")
    private String endDate;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "patientId")
    private Patient patient;

    @OneToMany(mappedBy = "medicationPlan", fetch = FetchType.EAGER)
    private List<MedicationToPlan> medicationToPlans;


    public MedicationPlan() {
    }

    public MedicationPlan(Integer medicationPlanId, String intakeInterval, String startDate, String endDate, Patient patient) {
        this.medicationPlanId = medicationPlanId;
        this.intakeInterval = intakeInterval;
        this.startDate = startDate;
        this.endDate = endDate;
        this.patient = patient;

    }

    public MedicationPlan(Integer medicationPlanId, String intakeInterval, String startDate, String endDate, Patient patient, List<MedicationToPlan> medicationToPlans) {
        this.medicationPlanId = medicationPlanId;
        this.intakeInterval = intakeInterval;
        this.startDate = startDate;
        this.endDate = endDate;
        this.patient = patient;
        this.medicationToPlans = medicationToPlans;
    }

    public MedicationPlan(Integer medicationPlanId, String intakeInterval, String startDate, String endDate) {
        this.medicationPlanId = medicationPlanId;
        this.intakeInterval = intakeInterval;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Integer getMedicationPlanId() {
        return medicationPlanId;
    }

    public void setMedicationPlanId(Integer medicationPlanId) {
        this.medicationPlanId = medicationPlanId;
    }

    public String getIntakeInterval() {
        return intakeInterval;
    }

    public void setIntakeInterval(String intakeInterval) {
        this.intakeInterval = intakeInterval;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public List<MedicationToPlan> getMedicationToPlans() {
        return medicationToPlans;
    }

    public void setMedicationToPlans(List<MedicationToPlan> medicationToPlans) {
        this.medicationToPlans = medicationToPlans;
    }
}
