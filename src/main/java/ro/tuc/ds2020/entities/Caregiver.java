package ro.tuc.ds2020.entities;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "caregiver")
public class Caregiver {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "caregiverId", unique = true, nullable = false)
    private Integer caregiverId;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "birthDate", nullable = false)
    private String birthDate;

    @Column(name = "gender")
    private String gender;

    @Column(name = "address")
    private String address;

    @OneToMany(mappedBy = "caregiver", fetch = FetchType.LAZY)
    private List<Patient> patients;

    public Caregiver() {
    }

    public Caregiver(Integer caregiverId, String name, String birthDate, String gender, String address,
                     List<Patient> patients) {
        super();
        this.caregiverId = caregiverId;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.patients = patients;
    }

    public Caregiver(Integer caregiverId, String name, String birthDate, String gender, String address) {
        super();
        this.caregiverId = caregiverId;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
    }

    public Caregiver(Integer caregiverId, String name) {

        this.caregiverId = caregiverId;
        this.name = name;
    }

    public Integer getCaregiverId() {
        return caregiverId;
    }

    public void setCaregiverId(Integer caregiverId) {
        this.caregiverId = caregiverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }
}
