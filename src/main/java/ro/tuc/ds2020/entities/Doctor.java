package ro.tuc.ds2020.entities;

import javax.persistence.*;

import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "doctor")
public class Doctor {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "doctorId", unique = true, nullable = false)
    private Integer doctorId;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "specialization")
    private String specialization;

    @Column(name = "birthdate")
    private String birthdate;

    @Column(name = "address")
    private String address;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "doctor")
    private List<Patient> patients;

    public Doctor() {
    }

    public Doctor(Integer doctorId, String name) {
        this.doctorId = doctorId;
        this.name = name;
    }

    public Doctor(Integer doctorId, String name, String specialization, String birthdate,
                  String address, List<Patient> patients) {
        this.doctorId = doctorId;
        this.name = name;
        this.specialization = specialization;
        this.birthdate = birthdate;
        this.address = address;
        this.patients = patients;
    }

    public Doctor(Integer doctorId, String name, String specialization, String birthdate, String address) {
        this.doctorId = doctorId;
        this.name = name;
        this.specialization = specialization;
        this.birthdate = birthdate;
        this.address = address;
    }


    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }
}
