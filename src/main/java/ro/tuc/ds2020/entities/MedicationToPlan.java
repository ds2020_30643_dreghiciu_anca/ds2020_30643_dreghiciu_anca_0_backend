package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medicationToPlan")
public class MedicationToPlan {


    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "medicationToPlanId", unique = true, nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name="medicationId", nullable = false)
    private Medication medication;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="medication_plan_id", nullable = false)
    private MedicationPlan medicationPlan;


    @Column(name="intakeInterval")
    private String intakeInterval;


    @Column(name="dosage")
    private String dosage;

    public MedicationToPlan() {
    }

    public MedicationToPlan(Integer id, Medication medication, String intakeInterval, String dosage) {
        this.id = id;
        this.medication = medication;
        this.intakeInterval = intakeInterval;
        this.dosage = dosage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setDrug(Medication medication) {
        this.medication = medication;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public String getIntakeInterval() {
        return intakeInterval;
    }

    public void setIntakeInterval(String intakeInterval) {
        this.intakeInterval = intakeInterval;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }


}
