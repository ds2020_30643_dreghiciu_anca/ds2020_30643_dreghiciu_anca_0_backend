package ro.tuc.ds2020.entities;

import javax.persistence.*;
import java.security.Identity;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication")
public class Medication {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "medicationId", unique = true, nullable = false)
    private Integer medicationId;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "sideEffects")
    private String sideEffects;

    @Column(name = "dosage")
    private String dosage;

    @OneToMany(mappedBy = "medication")
    private List<MedicationToPlan> medicationToPlans;

    public Medication() {
    }

    public Medication(Integer medicationId, String name) {
        this.medicationId = medicationId;
        this.name = name;
    }

    public Medication(Integer medicationId, String name, String sideEffects, String dosage) {
        super();
        this.medicationId = medicationId;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public Medication(String name, String sideEffects, String dosage) {
        super();
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public Medication(Integer medicationId, String name, String sideEffects, String dosage, List<MedicationToPlan> medicationToPlans) {
        this.medicationId = medicationId;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
        this.medicationToPlans = medicationToPlans;
    }

    public Integer getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(Integer medicationId) {
        this.medicationId = medicationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public List<MedicationToPlan> getMedicationToPlans() {
        return medicationToPlans;
    }

    public void setMedicationToPlans(List<MedicationToPlan> medicationToPlans) {
        this.medicationToPlans = medicationToPlans;
    }
}
