package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.*;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.DoctorRepository;
import ro.tuc.ds2020.repositories.PatientRepository;
import ro.tuc.ds2020.repositories.PersonRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {
    private final PatientRepository patientRepository;
    private PersonRepository personRepository;
    private CaregiverRepository caregiverRepository;
    private DoctorRepository doctorRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository, CaregiverRepository caregiverRepository, DoctorRepository doctorRepository)
    {

        this.patientRepository = patientRepository;
        this.caregiverRepository = caregiverRepository;
        this.doctorRepository = doctorRepository;
        //this.userRepository = userRepository;
    }

    public List<PatientDTO> findAll()
    {
        List<Patient> patientList = patientRepository.findAll();
        return patientList.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public PatientDTO findPatientById(Integer id)
    {
        Optional<Patient> patient = patientRepository.findById(id);
        if (!patient.isPresent())
        {
            throw new ResourceNotFoundException("patient"+"patientid"+id);
        }
        return PatientBuilder.generateDTOFromEntity(patient.get());
    }
    public Integer insert(PatientDTO patientDTO)
    {
        Patient patient = PatientBuilder.generateEntityFromDTO(patientDTO);
        Caregiver caregiver = caregiverRepository.findById(patientDTO.getCaregiverId()).get();
        caregiver.setName(patientDTO.getCaregiverName());

        Doctor doctor = doctorRepository.findById(patientDTO.getDoctorId()).get();
        doctor.setName(patientDTO.getDoctorName());
        patient.setCaregiver(caregiver);
        patient.setDoctor(doctor);
        patient = patientRepository.save(patient);
        //LOGGER.debug("Person with id" +person.getId() + "was inserted in db");
        return patient.getPatientId();
    }
    public void delete(int patientId)
    {
        this.patientRepository.deleteById(patientId);
    }
    public Integer update(Integer id, PatientDTO patientDTO)
    {
        Patient patient = PatientBuilder.generateEntityFromDTO(patientDTO);
        Caregiver caregiver = caregiverRepository.findById(patientDTO.getCaregiverId()).get();
        caregiver.setName(patientDTO.getCaregiverName());

        Doctor doctor = doctorRepository.findById(patientDTO.getDoctorId()).get();
        doctor.setName(patientDTO.getDoctorName());
        patient.setCaregiver(caregiver);
        patient.setDoctor(doctor);
        patient.setPatientId(id);
        patient = patientRepository.save(patient);
        //LOGGER.debug("Person with id" +person.getId() + "was inserted in db");
        return patient.getPatientId();

    }
   public PatientDTO getPatientByPersonId(Integer id)
    {
        Optional<Person> person = personRepository.findWithId(id);

        if(!person.isPresent())
        {
            throw new ResourceNotFoundException("error"+id);
        }
        return PatientBuilder.generateDTOFromEntity(person.get().getPatient());
    }


}
