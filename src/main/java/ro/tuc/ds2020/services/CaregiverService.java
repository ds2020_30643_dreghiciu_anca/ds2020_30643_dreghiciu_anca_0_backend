package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.PatientRepository;
import ro.tuc.ds2020.repositories.PersonRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@Service
public class CaregiverService {
    private final CaregiverRepository caregiverRepository;
    private final PatientRepository patientRepository;
    private final PersonRepository personRepository;
    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository, PatientRepository patientRepository,PersonRepository personRepository)
    {
        this.caregiverRepository = caregiverRepository;
        this.patientRepository=patientRepository;
        this.personRepository= personRepository;
    }

    public List<CaregiverDTO> findAll() {
        List<Caregiver> caregivers = caregiverRepository.findAll();
        List<CaregiverDTO> caregiverDTOs = new ArrayList<>();

        for (Caregiver caregiver : caregivers) {
            caregiverDTOs.add(CaregiverBuilder.generateDTOFromEntity(caregiver));
        }
        return caregiverDTOs;
    }

    public CaregiverDTO findCaregiverById(Integer id) {
        Optional<Caregiver> caregiver = caregiverRepository.findById(id);
        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver"+"caregiver id"+id);
        }
        return CaregiverBuilder.generateDTOFromEntity(caregiver.get());

    }

    public Integer insert(CaregiverDTO caregiverDTO) {
        Caregiver caregiver = CaregiverBuilder.generateEntityFromDTO(caregiverDTO);
        caregiver = caregiverRepository.save(caregiver);
        //LOGGER.debug("Person with id"+person.getId() +" was inserted in db");
        return caregiver.getCaregiverId();    }

    public void delete(int caregiverId)
    {
        this.caregiverRepository.deleteById(caregiverId);
    }

    public Integer update (Integer id, CaregiverDTO caregiverDTO)
    {
        Caregiver caregiver = CaregiverBuilder.generateEntityFromDTO(caregiverDTO);
        caregiver.setCaregiverId(id);
        caregiver = caregiverRepository.save(caregiver);
        //LOGGER.debug("Person with id" +person.getId() + "was inserted in db");
        return caregiver.getCaregiverId();
    }

    public CaregiverDTO findCaregiverByUserId(int userId)
    {
        Optional<Person> person = personRepository.findById(userId);
        Optional<Caregiver> caregiver = caregiverRepository.findById(person.get().getId());
        if(!caregiver.isPresent())
        {
            throw new ResourceNotFoundException("User"+"user id"+userId);


        }

        return CaregiverBuilder.generateDTOFromEntity(caregiver.get());

    }
}
