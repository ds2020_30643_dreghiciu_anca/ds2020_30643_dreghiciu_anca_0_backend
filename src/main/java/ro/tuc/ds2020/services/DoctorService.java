package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.DoctorDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.builders.DoctorBuilder;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.DoctorRepository;
import ro.tuc.ds2020.repositories.PersonRepository;

import javax.print.Doc;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DoctorService {
    private final DoctorRepository doctorRepository;
    private final PersonRepository personRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository, PersonRepository personRepository) {

        this.doctorRepository = doctorRepository;
        this.personRepository = personRepository;
    }

    public DoctorDTO findDoctorById(Integer id){
        Optional<Doctor> doctor  = doctorRepository.findById(id);

        if(!doctor.isPresent()) {
            throw new ResourceNotFoundException("Doctor" + "doctor id"+id);
        }
        return DoctorBuilder.generateDTOFromEntity(doctor.get());
    }
    public DoctorDTO findDoctorByUserId(Integer userId)
    {
        Optional<Person> person = personRepository.findById(userId);
        Optional<Doctor> doctor = doctorRepository.findById(person.get().getId());

        return DoctorBuilder.generateDTOFromEntity(doctor.get());

    }
    public List<DoctorDTO> findDoctors() {
        List<Doctor> doctorList = doctorRepository.findAll();
        return doctorList.stream()
                .map(DoctorBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(DoctorDTO doctorDTO) {
        Doctor doctor = DoctorBuilder.toEntity(doctorDTO);
        doctor = doctorRepository.save(doctor);
        //LOGGER.debug("Person with id"+person.getId() +" was inserted in db");
        return doctor.getDoctorId();
    }
    public Integer update(Integer id, DoctorDTO doctorDTO)
    {

        Doctor doctor = DoctorBuilder.toEntity(doctorDTO);
        doctor.setDoctorId(id);
        doctor = doctorRepository.save(doctor);
        //LOGGER.debug("Person with id" +person.getId() + "was inserted in db");
        return doctor.getDoctorId();
    }

    public void delete(int doctorId)
    {
        this.doctorRepository.deleteById(doctorId);
    }
}
