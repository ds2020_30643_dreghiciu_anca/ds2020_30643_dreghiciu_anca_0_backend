package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.MedicationRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class MedicationService {
    private final MedicationRepository medicationRepository;
    @Autowired
    public MedicationService(MedicationRepository medicationRepository)
    {
        this.medicationRepository=medicationRepository;
    }
    public List<MedicationDTO> findAll()
    {
        List<Medication> medications = medicationRepository.findAll();
        List<MedicationDTO> medicationDTOs = new ArrayList<>();
        for(Medication medication : medications)
        {
            medicationDTOs.add(MedicationBuilder.generateDTOFromEntity(medication));
        }
        return medicationDTOs;
    }

    public MedicationDTO findMedicationById(Integer id)
    {
        Optional<Medication> medication = medicationRepository.findById(id);
        if(!medication.isPresent())
        {
            throw new ResourceNotFoundException("Medication"+"medication id"+id);
        }
        return MedicationBuilder.generateDTOFromEntity(medication.get());
    }

    public Integer insert(MedicationDTO medicationDTO)
    {
        //return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medicationDTO)).getMedicationId();
        Medication medication = MedicationBuilder.generateEntityFromDTO(medicationDTO);
        medication = medicationRepository.save(medication);
        //LOGGER.debug("Person with id"+person.getId() +" was inserted in db");
        return medication.getMedicationId();
    }

    public void delete(int medicationId)
    {
        this.medicationRepository.deleteById(medicationId);

    }

    public Integer update(Integer id, MedicationDTO medicationDTO)
    {

        Medication medication = MedicationBuilder.generateEntityFromDTO(medicationDTO);
        medication.setMedicationId(id);
        medicationRepository.save(medication);

        return medication.getMedicationId();
    }
}
