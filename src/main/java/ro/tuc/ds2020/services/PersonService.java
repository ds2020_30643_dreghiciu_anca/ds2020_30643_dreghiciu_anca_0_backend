package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.PersonRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PersonService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final PersonRepository personRepository;

    @Autowired
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<PersonDTO> findPersons() {
        List<Person> personList = personRepository.findAll();
        return personList.stream()
                .map(PersonBuilder::toPersonDTO)
                .collect(Collectors.toList());
    }

    public PersonDTO findPersonById(Integer id) {
        Optional<Person> prosumerOptional = personRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id"+ id +" was not found in db");
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }
        return PersonBuilder.toPersonDTO(prosumerOptional.get());
    }

    public Integer insert(PersonDetailsDTO personDTO) {
        Person person = PersonBuilder.toEntity(personDTO);
        person = personRepository.save(person);
        LOGGER.debug("Person with id"+person.getId() +" was inserted in db");
        return person.getId();
    }

    public void delete(int patientId)
    {
        this.personRepository.deleteById(patientId);
    }

    public Integer update(Integer id, PersonDetailsDTO personDTO)
    {

        Person person = PersonBuilder.toEntity(personDTO);
        person.setId(id);
        person = personRepository.save(person);
        LOGGER.debug("Person with id" +person.getId() + "was inserted in db");
        return person.getId();
    }


    public PersonDTO findUser(PersonDTO personDTO)
    {
        List<Person> users = personRepository.findAll();

        for(Person p: users)
        {
            if((p.getUsername().equals(personDTO.getUsername())) && (p.getPassword().equals(personDTO.getPassword())))
            {
                return PersonBuilder.toPersonDTO(p);
            }
        }
        throw new ResourceNotFoundException("User or password not found!");
    }


}
