package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.dtos.builders.MedicationToPlanBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.MedicationToPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.repositories.MedicationToPlanRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {
    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicationToPlanRepository medicationToPlanRepository;
    private final PatientRepository patientRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository, MedicationToPlanRepository medicationToPlanRepository,
    PatientRepository patientRepository)
    {
        this.medicationPlanRepository=medicationPlanRepository;
        this.medicationToPlanRepository = medicationToPlanRepository;
        this.patientRepository = patientRepository;
    }
    public List<MedicationPlanDTO> findAll()
    {
        List<MedicationPlan> medications = medicationPlanRepository.findAll();
        List<MedicationPlanDTO> medicationPlanDTOs = new ArrayList<>();
        for(MedicationPlan medication : medications)
        {
            medicationPlanDTOs.add(MedicationPlanBuilder.generateDTOFromEntity(medication));
        }
        return medicationPlanDTOs;
    }

    public MedicationPlanDTO findMedicationById(Integer id)
    {
        Optional<MedicationPlan> medicationPlan = medicationPlanRepository.findById(id);
        if(!medicationPlan.isPresent())
        {
            throw new ResourceNotFoundException("Medication"+"medication id" + id);
        }
        return MedicationPlanBuilder.generateDTOFromEntity(medicationPlan.get());
    }

    public Integer insert(MedicationPlanDTO medicationPlanDTO)
    {

        MedicationPlan medicationPlan = MedicationPlanBuilder.generateEntityFromDTO(medicationPlanDTO);

        medicationPlan.setPatient(patientRepository.findById(medicationPlanDTO.getPatientId()).get());

        medicationPlan = medicationPlanRepository.save(medicationPlan);

        List<MedicationToPlan> list = medicationPlanDTO.getMedicationToPlanDTOS().stream()
                .map(MedicationToPlanBuilder::toEntity)
                .collect(Collectors.toList());

        for(MedicationToPlan p: list) {
            p.setMedicationPlan(medicationPlan);
            p = medicationToPlanRepository.save(p);
        }


        return medicationPlan.getMedicationPlanId();

        //return medicationPlanRepository.save(MedicationPlanBuilder.generateEntityFromDTO(medicationPlanDTO)).getMedicationPlanId();
    }

    public void delete(int medicationPlanId)
    {
        this.medicationPlanRepository.deleteById(medicationPlanId);
    }

    /*
    public Integer update(MedicationPlanDTO medicationPlanDTO,Patient patient)
    {
        Optional<MedicationPlan> medication = medicationPlanRepository.findById(medicationPlanDTO);
        if(!medication.isPresent())
        {
            throw new ResourceNotFoundException("Medication"+"medication id"+ medicationPlanDTO.getMedicationId());
        }
        return medicationPlanRepository.save(MedicationPlanBuilder.generateEntityFromDTO(medicationPlanDTO)).getMedicationPlanId();
    }*/
}
