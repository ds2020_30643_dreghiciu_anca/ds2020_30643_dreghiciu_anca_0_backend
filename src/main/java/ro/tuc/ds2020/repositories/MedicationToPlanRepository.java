package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.tuc.ds2020.entities.MedicationToPlan;


import java.util.List;

@Repository
public interface MedicationToPlanRepository extends JpaRepository<MedicationToPlan, Integer> {


}
