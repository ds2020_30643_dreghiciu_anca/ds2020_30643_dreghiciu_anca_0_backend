package ro.tuc.ds2020.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.tuc.ds2020.entities.Doctor;


@Repository
public interface DoctorRepository extends JpaRepository<Doctor,Integer> {
    @Query(value = "SELECT d " +
            "FROM Doctor d " +
            "ORDER BY d.name")
    List<Doctor> getAllOrdered();
}
