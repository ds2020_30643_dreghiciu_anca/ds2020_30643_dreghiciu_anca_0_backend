package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.ds2020.entities.Person;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Repository
public interface PersonRepository extends JpaRepository<Person, Integer> {

    /**
     * Example: JPA generate Query by Field
     */
    List<Person> findByUsername(String username);

    @Query(value = "SELECT p " +
            "FROM Person p " +
            "WHERE p.id = :id")
    Optional<Person> findWithId(@Param("id") Integer id);

    /**
     * Example: Write Custom Query
     */
    @Query(value = "SELECT p " +
            "FROM Person p " +
            "WHERE p.username = :username " +
            "AND p.age >= 60  ")
    Optional<Person> findSeniorsByUsername(@Param("username") String username);


}
