package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.Person;

import java.util.List;
import java.util.Optional;

@Repository
public interface MedicationRepository extends JpaRepository<Medication,Integer> {

    @Query(value = "SELECT m " +
            "FROM Medication m " +
            "ORDER BY m.name")
    List<Medication> getAllOrdered();


    List<Medication> findByName(String name);

    @Query(value = "SELECT p " +
            "FROM Medication p " +
            "WHERE p.id = :id")
    Optional<Medication> findWithId(@Param("id") Integer id);

    /**
     * Example: Write Custom Query
     */
    @Query(value = "SELECT p " +
            "FROM Medication p " +
            "WHERE p.name = :name "
            )
    Optional<Medication> findSeniorsByUsername(@Param("name") String name);
}
