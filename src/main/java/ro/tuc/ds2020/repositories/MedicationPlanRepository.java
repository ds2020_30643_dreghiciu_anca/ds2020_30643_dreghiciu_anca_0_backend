package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.tuc.ds2020.entities.MedicationPlan;

import java.util.List;

@Repository
public interface MedicationPlanRepository extends JpaRepository<MedicationPlan,Integer> {
    @Query(value = "SELECT m " +
            "FROM Medication m ")
    List<MedicationPlan> getAll();

}
