package ro.tuc.ds2020.dtos;

public class DoctorDTO {
    private Integer doctorId;
    private String name;
    private String specialization;
    private String birthdate;
    private String address;
    private String patients;


    public DoctorDTO(Integer doctorId, String name, String specialization,String birthdate, String address, String patients) {
        this.doctorId = doctorId;
        this.name = name;
        this.specialization = specialization;
        this.birthdate = birthdate;
        this.address = address;
        this.patients = patients;
    }
    public DoctorDTO(Integer doctorId, String name) {
        this.doctorId = doctorId;
        this.name = name;
    }

    public DoctorDTO(Integer doctorId, String name, String specialization, String birthdate, String address) {
        this.doctorId = doctorId;
        this.name = name;
        this.specialization = specialization;
        this.birthdate = birthdate;
        this.address = address;
    }

    public DoctorDTO() {
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPatients() {
        return patients;
    }

    public void setPatients(String patients) {
        this.patients = patients;
    }
}
