package ro.tuc.ds2020.dtos;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.List;

public class PatientDTO {
    private Integer id;
    private String name;
    private String birthDate;
    private String gender;
    private String address;
    private String medicalRecord;
    private Integer caregiverId;
    private String caregiverName;
    private Integer personId;
    private String username;
    private String password;
    List<MedicationPlanDTO> medications;
    private Integer doctorId;
    private String doctorName;

    public PatientDTO(Integer id, String name, String birthDate, String gender, String address, String medicalRecord,
                      Integer caregiverId, String caregiverName, Integer personId, String username, String password,
                      Integer doctorId, String doctorName) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.caregiverId = caregiverId;
        this.caregiverName = caregiverName;
        this.personId = personId;
        this.username = username;
        this.password = password;
        this.doctorId = doctorId;
        this.doctorName = doctorName;
    }

    public PatientDTO(Integer id, String name, String birthDate, String gender, String address, String medicalRecord) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
    }

    public PatientDTO(Integer id, String name, String birthDate, String gender, String address, String medicalRecord,
                      Integer caregiverId, String caregiverName, Integer personId, String username, String password,
                      List<MedicationPlanDTO> medications, Integer doctorId, String doctorName) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.caregiverId = caregiverId;
        this.caregiverName = caregiverName;
        this.personId = personId;
        this.username = username;
        this.password = password;
        this.medications = medications;
        this.doctorId = doctorId;
        this.doctorName = doctorName;
    }

    public PatientDTO(Integer id, String name, String birthDate, String gender, String address, String medicalRecord, Integer caregiverId, String caregiverName, Integer doctorId, String doctorName) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.caregiverId = caregiverId;
        this.caregiverName = caregiverName;
        this.doctorId = doctorId;
        this.doctorName = doctorName;
    }

    public PatientDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public Integer getCaregiverId() {
        return caregiverId;
    }

    public void setCaregiverId(Integer caregiverId) {
        this.caregiverId = caregiverId;
    }

    public String getCaregiverName() {
        return caregiverName;
    }

    public void setCaregiverName(String caregiverName) {
        this.caregiverName = caregiverName;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<MedicationPlanDTO> getMedications() {
        return medications;
    }

    public void setMedications(List<MedicationPlanDTO> medications) {
        this.medications = medications;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public int getAge()
    {
        return 1000;
    }
}
