package ro.tuc.ds2020.dtos;

import java.sql.Date;
import java.util.List;

public class CaregiverDetailsDTO {
    private Integer id;
    private String name;
    private String birthDate;
    private String gender;
    private String address;

    private List<PatientDTO> patients;

    public CaregiverDetailsDTO() {}

    public CaregiverDetailsDTO(Integer id, String name, String birthDate, String gender, String address,
                                    List<PatientDTO> patients) {
        super();
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.patients = patients;
    }

    public CaregiverDetailsDTO(Integer id, String name, String birthDate, String gender, String address) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<PatientDTO> getPatients() {
        return patients;
    }

    public void setPatients(List<PatientDTO> patients) {
        this.patients = patients;
    }
}
