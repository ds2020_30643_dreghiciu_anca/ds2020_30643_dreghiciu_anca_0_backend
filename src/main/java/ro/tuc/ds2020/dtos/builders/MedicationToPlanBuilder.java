package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationToPlanDTO;
import ro.tuc.ds2020.entities.MedicationToPlan;

public class MedicationToPlanBuilder {



    public static MedicationToPlanDTO toMedicationToPlanDTO(MedicationToPlan medicationToPlan) {

        return new MedicationToPlanDTO(medicationToPlan.getId(),
                MedicationBuilder.generateDTOFromEntity(medicationToPlan.getMedication()),
                medicationToPlan.getIntakeInterval(),
                medicationToPlan.getDosage()
        );

    }

    public static MedicationToPlan toEntity(MedicationToPlanDTO medicationToPlanDTO) {
        return new MedicationToPlan(
                medicationToPlanDTO.getId(),
                MedicationBuilder.generateEntityFromDTO(medicationToPlanDTO.getMedicationDTO()),
                medicationToPlanDTO.getIntakeInterval(),
                medicationToPlanDTO.getDosage()
        );
    }
}
