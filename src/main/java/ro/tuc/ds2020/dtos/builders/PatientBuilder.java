package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.entities.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PatientBuilder {
    public static PatientDTO generateDTOFromEntity(Patient patient){

        return new PatientDTO(
                patient.getPatientId(),
                patient.getName(),
                patient.getBirthDate(),
                patient.getGender(),
                patient.getAddress(),
                patient.getMedicalRecord(),
                patient.getCaregiver().getCaregiverId(),
                patient.getCaregiver().getName(),
                patient.getDoctor().getDoctorId(),
                patient.getDoctor().getName()
        );
    }

    public static Patient generateEntityFromDTO(PatientDTO patientDTO){

        return new Patient(
                patientDTO.getId(),
                patientDTO.getName(),
                patientDTO.getBirthDate(),
                patientDTO.getGender(),
                patientDTO.getAddress(),
                patientDTO.getMedicalRecord()


        );

    }
}
