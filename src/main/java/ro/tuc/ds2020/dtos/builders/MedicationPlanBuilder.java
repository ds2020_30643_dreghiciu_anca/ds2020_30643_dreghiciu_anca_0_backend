package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.MedicationToPlanDTO;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.MedicationToPlan;
import ro.tuc.ds2020.entities.Patient;

import java.util.ArrayList;
import java.util.List;

public class MedicationPlanBuilder {

    public static MedicationPlanDTO generateDTOFromEntity (MedicationPlan medicationPlan)
    {
        List<MedicationToPlanDTO> dtos = new ArrayList<>();
        for(MedicationToPlan p: medicationPlan.getMedicationToPlans()) {
            dtos.add(MedicationToPlanBuilder.toMedicationToPlanDTO(p));
        }
        return new MedicationPlanDTO(medicationPlan.getMedicationPlanId(),
                medicationPlan.getIntakeInterval(),
                medicationPlan.getStartDate(),
                medicationPlan.getEndDate(),
                medicationPlan.getPatient().getPatientId(),
                dtos);

    }



    public static MedicationPlan generateEntityFromDTO(MedicationPlanDTO medicationPlanDTO)
    {return new MedicationPlan(medicationPlanDTO.getMedicationPlanId(),
            medicationPlanDTO.getIntakeInterval(),
            medicationPlanDTO.getStartDate(),
            medicationPlanDTO.getEndDate()
    );
    }
}


