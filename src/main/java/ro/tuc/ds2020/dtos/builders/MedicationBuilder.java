package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.entities.Medication;

public class MedicationBuilder {

    public static MedicationDTO generateDTOFromEntity (Medication medication)
    {
        return new MedicationDTO(
                medication.getMedicationId(),
                medication.getName(),
                medication.getSideEffects(),
                medication.getDosage()
        );
    }
    public static Medication generateEntityFromDTO (MedicationDTO medicationDTO)
    {
        return new Medication(
                medicationDTO.getId(),
                medicationDTO.getName(),
                medicationDTO.getSideEffects(),
                medicationDTO.getDosage()

        );

    }


}

/*
public DrugBuilder () {

    }

    public static DrugDTO toDrugDTO(Drug drug) {
        return new DrugDTO(drug.getId(),
                drug.getName(),
                drug.getConcentrations(),
                drug.getSideEffects());
    }

    public static Drug toEntity(DrugDTO drugDTO) {

        return new Drug(drugDTO.getId(),
                drugDTO.getName(),
                drugDTO.getConcentrations(),
                drugDTO.getSideEffects());
    }
 */
