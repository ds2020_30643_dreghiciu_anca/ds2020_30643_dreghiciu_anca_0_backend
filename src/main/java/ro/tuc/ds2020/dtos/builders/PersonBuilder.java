package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.entities.Person;

public class PersonBuilder {

    private PersonBuilder() {
    }

    public static PersonDTO toPersonDTO(Person person) {
        return new PersonDTO(
                person.getId(),
                person.getUsername(),
                person.getAddress(),
                person.getPassword(),
                person.getAge(),
                person.getEmail(),
                person.getRole());
    }

    public static Person toEntity(PersonDetailsDTO personDetailsDTO) {
        return new Person(personDetailsDTO.getUsername(),
                personDetailsDTO.getAddress(),
                personDetailsDTO.getPassword(),
                personDetailsDTO.getAge(),
                personDetailsDTO.getEmail(),
                personDetailsDTO.getRole());
    }
}
