package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.DoctorDTO;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Person;

public class DoctorBuilder {

    public static DoctorDTO generateDTOFromEntity(Doctor doctor)
    {
        return new DoctorDTO(
                doctor.getDoctorId(),
                doctor.getName(),
                doctor.getSpecialization(),
                doctor.getBirthdate(),
                doctor.getAddress()

        );
    }

    /*
     this.doctorId = doctorId;
        this.name = name;
        this.specialization = specialization;
        this.birthdate = birthdate;
        this.address = address;
     */

    public static Doctor toEntity(DoctorDTO doctorDTO)
    {
        return new Doctor(
              doctorDTO.getDoctorId(),
              doctorDTO.getName(),
              doctorDTO.getSpecialization(),
              doctorDTO.getBirthdate(),
              doctorDTO.getAddress()
        );
    }


}
