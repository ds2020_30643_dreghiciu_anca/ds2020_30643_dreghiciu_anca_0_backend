package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.entities.Person;

public class PersonDetailsBuilder {

    public static PersonDetailsDTO generateDTOFromEntity(Person person){
        return new PersonDetailsDTO(
                person.getId(),
                person.getUsername(),
                person.getAddress(),
                person.getPassword(),
                person.getAge(),
                person.getEmail(),
                person.getRole()
        );
    }
   //Integer id, String username, String address,String password, int age, String email, String role)

    public static Person generateEntityFromDTO(PersonDetailsDTO personDetailsDTO){
        return new Person(
                personDetailsDTO.getId(),
                personDetailsDTO.getUsername(),
                personDetailsDTO.getAddress(),
                personDetailsDTO.getPassword(),
                personDetailsDTO.getAge(),
                personDetailsDTO.getEmail(),
                personDetailsDTO.getRole()
        );
    }
    //Integer id, String username, String address, String password, int age, String email, String role)
}
