package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.entities.Caregiver;

public class CaregiverBuilder {
    public static CaregiverDTO generateDTOFromEntity(Caregiver caregiver)
    {
        return new CaregiverDTO(caregiver.getCaregiverId(),
                caregiver.getName(),
                caregiver.getBirthDate(),
                caregiver.getGender(),
                caregiver.getAddress()

        );
    }
    //Integer caregiverId, String name, Date birthDate, String gender, String address

    public static Caregiver generateEntityFromDTO(CaregiverDTO caregiverDTO)
    {
        return new Caregiver(
                caregiverDTO.getId(),
                caregiverDTO.getName(),
                caregiverDTO.getBirthDate(),
                caregiverDTO.getGender(),
                caregiverDTO.getAddress()
        );

    }

}
