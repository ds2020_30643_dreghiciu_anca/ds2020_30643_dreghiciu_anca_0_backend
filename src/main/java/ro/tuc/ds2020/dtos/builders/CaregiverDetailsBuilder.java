package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;

import java.util.ArrayList;
import java.util.List;

public class CaregiverDetailsBuilder {

    public static CaregiverDetailsDTO generateDTOFromEntity(Caregiver caregiver)
    {

        return new CaregiverDetailsDTO(caregiver.getCaregiverId(),caregiver.getName(),caregiver.getBirthDate(),
                caregiver.getGender(),caregiver.getAddress());


    }
    //Integer id, String name, Date birthDate, String gender, String address, String medicalRecord,
    //                      Integer caregiverId, String caregiverName, Integer personId, String username, String password,
    //                      List<MedicationPlanDTO> medications, Integer doctorId, String doctorName



    //Integer id, String name, Date birthDate, String gender, String address,
    //                                    List<PatientDTO> patients
}
