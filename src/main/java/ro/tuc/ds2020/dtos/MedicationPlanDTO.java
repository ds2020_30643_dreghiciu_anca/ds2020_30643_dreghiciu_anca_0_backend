package ro.tuc.ds2020.dtos;

import java.sql.Date;
import java.util.List;

public class MedicationPlanDTO {
    private Integer medicationPlanId;
    private String intakeInterval;
    private String startDate;
    private String endDate;


    private Integer patientId;
    private List<MedicationToPlanDTO> medicationToPlanDTOS;

    public MedicationPlanDTO(Integer medicationPlanId, String intakeInterval, String startDate, String endDate,
                             Integer patientId, List<MedicationToPlanDTO> medicationToPlanDTOS) {
        this.medicationPlanId = medicationPlanId;
        this.intakeInterval = intakeInterval;
        this.startDate = startDate;
        this.endDate = endDate;
        this.patientId = patientId;
        this.medicationToPlanDTOS = medicationToPlanDTOS;

    }



    public Integer getMedicationPlanId() {
        return medicationPlanId;
    }

    public void setMedicationPlanId(Integer medicationPlanId) {
        this.medicationPlanId = medicationPlanId;
    }

    public String getIntakeInterval() {
        return intakeInterval;
    }

    public void setIntakeInterval(String intakeInterval) {
        this.intakeInterval = intakeInterval;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public List<MedicationToPlanDTO> getMedicationToPlanDTOS() {
        return medicationToPlanDTOS;
    }

    public void setMedicationToPlanDTOS(List<MedicationToPlanDTO> medicationToPlanDTOS) {
        this.medicationToPlanDTOS = medicationToPlanDTOS;
    }
}
