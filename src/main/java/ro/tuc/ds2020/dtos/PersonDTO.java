package ro.tuc.ds2020.dtos;

import java.util.Objects;
import java.util.UUID;

public class PersonDTO {
    private Integer id;
    private String username;
    private String address;
    private String password;
    private int age;
    private String role;
    private String email;


    public PersonDTO() {
    }

    public PersonDTO(Integer id, String username, String address,String password, int age, String email, String role) {
        this.id = id;
        this.username = username;
        this.address = address;
        this.password = password;
        this.age = age;
        this.email = email;
        this.role = role;
    }

    public PersonDTO(String username, String address, String password, int age, String role, String email) {
        this.username = username;
        this.address = address;
        this.password = password;
        this.age = age;
        this.role = role;
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void getUsername(String name) {
        this.username = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonDTO personDTO = (PersonDTO) o;
        return age == personDTO.age &&
                Objects.equals(username, personDTO.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, age);
    }
}
