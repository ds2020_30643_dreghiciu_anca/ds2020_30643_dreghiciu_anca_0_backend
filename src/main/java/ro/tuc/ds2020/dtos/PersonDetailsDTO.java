package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.dtos.validators.annotation.AgeLimit;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class PersonDetailsDTO {

    private Integer id;
    @NotNull
    private String username;
    @NotNull
    private String address;
    @NotNull
    private String password;
    @AgeLimit(limit = 18)
    private int age;
    @NotNull
    private String email;
    @NotNull
    private String role;

    public PersonDetailsDTO() {
    }


    public PersonDetailsDTO( String username, String address,String password, int age, String email, String role) {
        this.username = username;
        this.password = password;
        this.age = age;
        this.email = email;
        this.role = role;
    }

    public PersonDetailsDTO(Integer id, String username, String address,String password, int age, String email, String role) {
        this.id = id;
        this.username = username;
        this.address = address;
        this.password = password;
        this.age = age;
        this.email = email;
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
