package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.Medication;

public class MedicationToPlanDTO {

    private Integer id;
    private MedicationDTO medicationDTO;

    private String intakeInterval;

    private String dosage;


    public MedicationToPlanDTO() {
    }

    public MedicationToPlanDTO(Integer id, MedicationDTO medication, String intakeInterval, String dosage) {
        this.id = id;
        this.medicationDTO = medication;
        this.intakeInterval = intakeInterval;
        this.dosage = dosage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MedicationDTO getMedicationDTO() {
        return medicationDTO;
    }

    public void setMedicationDTO(MedicationDTO medicationDTO) {
        this.medicationDTO = medicationDTO;
    }

    public String getIntakeInterval() {
        return intakeInterval;
    }

    public void setIntakeInterval(String intakeInterval) {
        this.intakeInterval = intakeInterval;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }


}
