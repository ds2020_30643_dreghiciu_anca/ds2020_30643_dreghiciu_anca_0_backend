package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.services.PatientService;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {
    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService)
    {
        this.patientService = patientService;
    }
    @GetMapping(value = "/{id}")
    public PatientDTO findByID(@PathVariable("id") Integer id)
    {
        return patientService.findPatientById(id);
    }

    @GetMapping()
    public List<PatientDTO> findAll()
    {
        return patientService.findAll();
    }

    @PostMapping
    public Integer insertPatientDTO(@RequestBody PatientDTO patientDTO)
    {
        return patientService.insert(patientDTO);
    }

    @DeleteMapping("/{id}")
    public Integer delete(@PathVariable("id") Integer id)
    {
        patientService.delete(id);
        return id;
    }

    @PutMapping("/{id}")
    public Integer update(@PathVariable("id") Integer id, @RequestBody PatientDTO patientDTO)
    {
        return patientService.update(id, patientDTO);
    }

    @GetMapping("/patient/{id}")
    public PatientDTO getPatientByPersonId(@PathVariable("id")Integer id)
    {
        return patientService.getPatientByPersonId(id);
    }
}
