package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.services.CaregiverService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {
    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService)
    {
        this.caregiverService=caregiverService;
    }

    @GetMapping()
    public List<CaregiverDTO> findAll()
    {
        return caregiverService.findAll();
    }

    @PostMapping
    public Integer insertCaregiverDTO(@RequestBody CaregiverDTO caregiverDTO)
    {
        return caregiverService.insert(caregiverDTO);
    }

    @DeleteMapping("/{id}")
    public Integer delete(@PathVariable("id") Integer id)
    {
        caregiverService.delete(id);
        return id;
    }

    @PutMapping("/{id}")
    public Integer update(@PathVariable Integer id, @RequestBody CaregiverDTO caregiverDTO)
    {
        return caregiverService.update(id, caregiverDTO);
    }

    @GetMapping("/caregiver/{userId}")
    public CaregiverDTO getCaregiverByUserId(@PathVariable("userId") Integer id)
    {
        return caregiverService.findCaregiverByUserId(id);
    }
}
