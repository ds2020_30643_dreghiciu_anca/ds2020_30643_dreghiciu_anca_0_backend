package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.services.MedicationService;


import java.util.List;
import java.util.logging.Logger;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {
    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService)
    {
        this.medicationService = medicationService;
    }

    @GetMapping()
    public List<MedicationDTO> findAll()
    {
        return medicationService.findAll();
    }

    @PostMapping()
    public ResponseEntity<Integer> insert(@RequestBody MedicationDTO medicationDTO) {
       Logger.getLogger("insert medication?");
        Integer medicationID = medicationService.insert(medicationDTO);
        return new ResponseEntity<>(medicationID, HttpStatus.CREATED);
    }
    @DeleteMapping("/{id}")
    public Integer delete(@PathVariable("id") Integer id)
    {
        medicationService.delete(id);
        return id;
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Integer> update(@PathVariable("id") Integer id, @RequestBody MedicationDTO medicationDTO){

        Integer medicationId = medicationService.update(id, medicationDTO);
        return new ResponseEntity<>(medicationId, HttpStatus.CREATED);
    }


}
