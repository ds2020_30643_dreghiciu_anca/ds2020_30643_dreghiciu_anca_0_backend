package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.services.CaregiverService;
import ro.tuc.ds2020.services.DoctorService;
import ro.tuc.ds2020.services.PatientService;
import ro.tuc.ds2020.services.PersonService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/person")
public class PersonController {

    private final PersonService personService;
    private final PatientService patientService;
    private final CaregiverService caregiverService;
    private final DoctorService doctorService;


    @Autowired
    public PersonController(PersonService personService, PatientService patientService,
                            CaregiverService caregiverService, DoctorService doctorService) {

        this.personService = personService;
        this.patientService = patientService;
        this.caregiverService = caregiverService;
        this.doctorService = doctorService;
    }

    @GetMapping()
    public ResponseEntity<List<PersonDTO>> getPersons() {
        List<PersonDTO> dtos = personService.findPersons();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Integer> insertPerson(@RequestBody PersonDetailsDTO personDTO) {
        Integer personID = personService.insert(personDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PersonDTO> getPerson(@PathVariable("id") Integer personId) {
        PersonDTO dto = personService.findPersonById(personId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Integer> updatePerson(@PathVariable("id") Integer id, @RequestBody PersonDetailsDTO personDetailsDTO){

        Integer personId = personService.update(id, personDetailsDTO);
        return new ResponseEntity<>(personId, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public Integer delete(@PathVariable("id") Integer id)
    {
        PersonDTO dto = personService.findPersonById(id);
        /*
        if(dto.getRole().equals("Patient"))
        {
            patientService.delete(id);
        }
        else if(dto.getRole().equals("Caregiver"))
            caregiverService.delete(id);
        else doctorService.delete(id);

         */

        personService.delete(id);
        return id;
    }



}
