package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.services.MedicationPlanService;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationPlan")
public class MedicationPlanController {
    private final MedicationPlanService medicationPlanService;
    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService)
    {
        this.medicationPlanService=medicationPlanService;
    }

    @GetMapping()
    public List<MedicationPlanDTO> findAll()
    {
        return medicationPlanService.findAll();
    }

    @GetMapping("/{id}")
    public MedicationPlanDTO findById(@PathVariable("id") Integer id){return medicationPlanService.findMedicationById(id);}

    @PostMapping
    public Integer insertCaregiverDTO(@RequestBody MedicationPlanDTO medicationPlanDTO)
    {
        return medicationPlanService.insert(medicationPlanDTO);
    }

    @DeleteMapping("/{id}")
    public Integer delete(@PathVariable("id") Integer id)
    {
        medicationPlanService.delete(id);
        return id;
    }

    /*@PutMapping
    public Integer update(@RequestBody MedicationPlanDTO medicationPlanDTO,Patient patient)
    {
        return medicationPlanService.update(medicationPlanDTO,patient);
    }*/
}
